
NAME := hello
TARGET := $(NAME).dll
VS_PATH := VintageStory
LIBS := -r:$(VS_PATH)/VintageStoryApi.dll \
	-r:$(VS_PATH)/Mods/VSEssentials.dll \
	-r:$(VS_PATH)/Mods/VSSurvivalMod.dll
PDB := $(NAME).pdb
ZIP := $(NAME).zip
OUT := bin/$(ZIP)
SRC := $(wildcard src/*.cs src/**/*.cs)

debug: $(OUT)

$(OUT): $(ZIP)
	cp $< $@

$(ZIP): target/$(TARGET) target/$(PDB) modinfo.json
	cp modinfo.json target;cd target;zip ../$@ $(TARGET) $(PDB) modinfo.json;cd ..


target/$(TARGET): $(SRC)
	csc -out:target/$(TARGET) -refout:target/$(TARGET).ref -t:library $(LIBS) -debug+ $^

clean:
	rm $(ZIP) target/*
